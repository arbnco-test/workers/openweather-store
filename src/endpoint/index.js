const Store = require('../db').api
const Router = require('express').Router()
const Logger = require('../utils/logger.js').Logger

module.exports = () => {
	Router.post('/endpoint', async (req, res) => {
		// Filter Bad Requests
		if (!req.body || !req.body.message || !req.body.message.data) {
			Logger.error('Bad request - Openweather store endpoint')
			return res.sendStatus(400)
		}

		const UTF8enc = Buffer.from(req.body.message.data, 'base64').toString('utf8')

		// Filter requests that aren't properly encoded
		var content
		try {
			content = JSON.parse(UTF8enc)
		} catch (err) {
			Logger.error('Bad request - Openweather store endpoint')
			return res.sendStatus(400)
		}

		// Setup Store
		Store.UseProd()
		Store.SetDB('weather')
		Store.SetCollection('forecasts')

		// Check if message contains a forecast
		if (content.forecast) {
			try {
				await Store.InsertOne(content.forecast)
			} catch (e) {
				Logger.error('Store Insert Error - Openweather store endpoint')
				return res.sendStatus(500)
			}
			return res.sendStatus(200)
		}

		Logger.error('Bad request no forecast in message - Openweather store endpoint')
		return res.sendStatus(400)
	})
	return Router
}
