const MongoClient = require('mongodb').MongoClient
const Logger = require('../utils/logger.js').Logger

// Defaults
var collection = 'forecasts'
var dbName = 'weather'
var url =
	'mongodb://prod:jYpM9k1vXGKOSijP@arbn-shard-00-00-nwvol.gcp.mongodb.net:27017,arbn-shard-00-01-nwvol.gcp.mongodb.net:27017,arbn-shard-00-02-nwvol.gcp.mongodb.net:27017/t?ssl=true&replicaSet=arbn-shard-0&authSource=admin&retryWrites=true'

// Config
const SetCollection = name => (collection = name)
const SetDB = name => (dbName = name)
const UseProd = () =>
	(url =
		'mongodb://prod:jYpM9k1vXGKOSijP@arbn-shard-00-00-nwvol.gcp.mongodb.net:27017,arbn-shard-00-01-nwvol.gcp.mongodb.net:27017,arbn-shard-00-02-nwvol.gcp.mongodb.net:27017/t?ssl=true&replicaSet=arbn-shard-0&authSource=admin&retryWrites=true')
const UseTest = () => (url = 'mongodb+srv://test:jnOUEhFT21vpIABn@arbn-nwvol.gcp.mongodb.net/t?retryWrites=true')

/// Method
const InsertOne = async obj => {
	let cli

	try {
		cli = await MongoClient.connect(url, { useNewUrlParser: false })

		return await cli
			.db(dbName)
			.collection(collection)
			.insertOne(obj)
	} catch (err) {
		Logger.error(err.message)
		throw err
	} finally {
		if (cli) {
			cli.close()
		}
	}
}

const api = {
	UseProd: UseProd,
	UseTest: UseTest,
	SetDB: SetDB,
	SetCollection: SetCollection,
	InsertOne: InsertOne
}

module.exports = { api }
