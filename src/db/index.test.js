const Store = require('./index.js').api

describe('InsertOne()', () => {
	it('it should fail if its not an object inserted', async () => {
		// connect to test database with test user
		Store.UseTest()
		Store.SetDB('weather-test')
		Store.SetCollection('forecasts')

		async function check() {
			await Store.InsertOne('this is a test')
		}
		await expect(check()).rejects.toThrow(Error)
	})
	it('it should insert a record into the production database', async () => {
		// connect to test database with production user
		Store.UseProd()
		Store.SetDB('weather-test')
		Store.SetCollection('forecasts')

		let resp = await Store.InsertOne({ test: 'this is a test' })

		expect(resp.insertedCount).toBe(1)
	})

	it('it should insert a record into the test database', async () => {
		/// connect to test database with test user
		Store.UseTest()
		Store.SetDB('weather-test')
		Store.SetCollection('forecasts')

		let resp = await Store.InsertOne({ test: 'this is a test' })

		expect(resp.insertedCount).toBe(1)
	})
})
