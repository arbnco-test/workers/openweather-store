const http = require('http')
const express = require('express')

const morgan = require('morgan')
const bodyParser = require('body-parser')

const Endpoint = require('./endpoint')
const config = require('./config.json')

let app = express()
app.server = http.createServer(app)

app.use(morgan('dev'))
app.use(bodyParser.json({ limit: config.bodyLimit }))

app.use('/', Endpoint())

app.server.listen(process.env.PORT || config.port, () => {
	console.log(`Started on port ${app.server.address().port}`)
})
