const Logger = require('../utils/logger.js').Logger
const { PubSub } = require('@google-cloud/pubsub')
const config = require('../config.json')

const pubsub = new PubSub()

const buf = obj => Buffer.from(JSON.stringify(obj))

const topic = async () => {
	try {
		let topic = await pubsub.createTopic(config.topic)
		return topic
	} catch (err) {
		return await pubsub.topic(config.topic)
	}
}

const publish = async obj => {
	const data = {
		forecast: obj
	}

	try {
		let t = await topic()
		let msgID = await t.publish(buf(data))

		Logger.info(`Forcast ${msgID} queued for background processing`)
		Logger.info(data)

		return msgID
	} catch (err) {
		Logger.error('Error occurred while queuing forecast', err)
	}
}

const api = {
	topic: topic,
	publish: publish
}

module.exports = { api }
